import React, { useState, useEffect /* , Suspense */ } from "react";
/* import { Skeleton } from "@material-ui/lab"; */
import maxBy from "lodash/maxBy";
import keys from "lodash.keys";
import results from "./data/results";
import {
	Switch,
	Route,
	/* Redirect, */
	useHistory,
	/* BrowserRouter, */
} from "react-router-dom";

import TestComponent from "./sections/TestComponent";
import EndResultComponent from "./sections/EndResultComponent";
import LoadingScreen from "./hooks/LoadingScreen";
/* const EndResultSection = React.lazy(() =>
	import("./sections/EndResultSection")
); */

export default function App() {
	const [currentQuestion, setCurrentQuestion] = useState(0);
	const [showEndResult, setShowEndResult] = useState(false);
	const [endResult, setEndResult] = useState("ikaantymisturva");
	const [endResultUrl, setEndResultUrl] = useState("");
	const [endResultTitle, setEndResultTitle] = useState("");
	const [endResultPath, setEndResultPath] = useState("/");
	const [endResultIndex, setEndResultIndex] = useState(0); // TODO
	const [showIntro, setShowIntro] = useState(true);
	const [score, setScore] = useState({
		unelmatNakyviksi: 0,
		kunJotainSattuu: 0,
		ennakoiElamasiEhtoota: 0,
		ikaantymisturva: 0,
	});

	/* // fake authenticate for pre-loading reasons
	const authenticate = () => {
		return new Promise((resolve) => setTimeout(resolve, 2000)); // 2 seconds
	};

	// removes pre-loaded-DOM-loading-screen
	useEffect(() => {
		authenticate().then(() => {
			const ele = document.getElementById("ipl-progress-indicator");
			if (ele) {
				// fade out
				ele.classList.add("available");
				setTimeout(() => {
					// remove from DOM
					ele.outerHTML = "";
				}, 2000);
			}
		});
	}, []); */

	// react-router useHistory for history reasons
	let history = useHistory();

	// for image preloading
	const endImageList = results.map(
		(resultImageSrc) => resultImageSrc.resultImg
	);

	// preloading images
	useEffect(() => {
		endImageList.forEach((image) => {
			new Image().src = image;
		});
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	// counts the end result category
	useEffect(() => {
		console.log("Score string: " + JSON.stringify(score));
		const countEndResult = maxBy(keys(score), function (o) {
			return score[o];
		});
		setEndResult(countEndResult);
		console.log("endResult pisteet: " + endResult);
	}, [showEndResult, endResult, score]);

	// calculates the index of end result
	useEffect(() => {
		const getEndResultIndex = results.findIndex(
			(result) => result.resultCategory === endResult
		);
		setEndResultIndex(getEndResultIndex);
	}, [score, endResult]);

	// sets the result to be rendered based on end result index
	useEffect(() => {
		setEndResultUrl(results[endResultIndex].resultUrl);
		setEndResultTitle(results[endResultIndex].resultTitle);
		setEndResultPath(results[endResultIndex].resultPath);
		console.log("endResult.js findEndResult: " + endResultIndex);
		console.log("endResult.js endResult: " + endResult);
	}, [score, endResult, endResultIndex]);

	/* 	useEffect(() => {
		history.push(endResultPath); //TODO how to not push on mounting
	}, [showEndResult]); */

	// scrolls window back to up after answering
	/* useEffect(() => {
		window.scrollTo({
			top: 0,
			left: 0,
			behavior: "smooth",
		});
	}, [currentQuestion, showEndResult]); */

	const handleStart = () => {
		setShowIntro(false);
	};

	const handleReset = () => {
		setScore({
			unelmatNakyviksi: 0,
			kunJotainSattuu: 0,
			ennakoiElamasiEhtoota: 0,
			ikaantymisturva: 0,
		});
		setShowIntro(true);
		setShowEndResult(false);
		setCurrentQuestion(0);
		console.log(score);
		setEndResultPath("/");
		setEndResultUrl("");
		setEndResultTitle("");
		setEndResult("ikaantymisturva");
		history.push("/");
	};

	LoadingScreen();

	return (
		<div className="app-wrapper">
			<div className="app">
				{/* <BrowserRouter /> */}
				<Switch>
					<Route path="/lopputulos">
						<EndResultComponent
							endResult={endResult}
							handleReset={handleReset}
							showEndResult={showEndResult}
							endResultUrl={endResultUrl}
							endResultTitle={endResultTitle}
							endResultPath={endResultPath}
							endResultIndex={endResultIndex}
							/* setEndResultPath={setEndResultPath} */
						/>
					</Route>

					<Route exact path="/">
						<TestComponent
							showIntro={showIntro}
							setShowIntro={setShowIntro}
							currentQuestion={currentQuestion}
							score={score}
							setScore={setScore}
							setCurrentQuestion={setCurrentQuestion}
							setShowEndResult={setShowEndResult}
							endResult={endResult}
							endResultPath={endResultPath}
							setEndResult={setEndResult}
							showEndResult={showEndResult}
							handleReset={handleReset}
							handleStart={handleStart}
						/>
					</Route>
				</Switch>
				{/* </BrowserRouter> */}
			</div>
		</div>
	);
}

import React /* , { useState, useEffect } */ from "react";
import { useTransition, useSpring, animated, config } from "react-spring";
/* import { useMeasure } from "react-use"; */

const AnimatedButtonImage = ({ imageSrc, onClick, alt }) => {
	const bgTransition = useSpring({
		/* height: "50px",
		maxHeight: "50px", */
		config: { tension: 170, friction: 24 },
	});

	const imageTransition = useTransition(imageSrc, alt, (item) => item, {
		from: { opacity: 0 },
		enter: { opacity: 1 },
		leave: { opacity: 0 },
		config: config.gentle,
	});

	// className="img-answer-container">
				// animation here

	return (
		// <div className="animation-wrapper">
		<animated.button
			className="answer-img-button"
			style={bgTransition}
			onClick={onClick}
		>
			<div className="test">
			{imageTransition.map(({ item, props, key, alt }) => (
				<animated.img className="answer-img" style={props} key={key} src={item} alt={alt} ></animated.img>
			))}
			</div>
		</animated.button>
		// </div>
	);
};

export default AnimatedButtonImage;

	

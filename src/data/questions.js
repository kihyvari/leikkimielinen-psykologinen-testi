const questions = [
	/* {
		// intro
		questionText: "Mikä on elämäsi tarkoitus, siinäpä vasta pulma. Oletko enemmän oman elämäsi kuski vai kyytiläinen? Ota siitä selvää.",
		questionURL: "assets/comp-intro.jpg",
		questionAlt: "Matkustajia lavapakettiauton kyydissä",
		answerOptions: [
			{
				answerText: "Aloita Testi!",
				category: "N/A",
			},
		],
	}, */
	{
		// question #1
		questionText: "Näet mustan kissan ylittämässä tietä. Miten reagoit?",
		questionURL: "assets/comp-question-1.jpg",
		questionAlt: "Musta kissa tuijottamassa",
		answerOptions: [
			{
				answerText: "Jarrutat ja odotat kissan ylittävän tien rauhassa.",
				category: "Unelmat näkyväksi",
				id: "1"
			},
			{
				answerText:
					"Säikähdät pahanpäiväisesti ja suoritat varotoimenpiteenä rituaalin, jossa sylkäiset ja heität suolaa olkasi yli.",
				category: "Kun jotain sattuu",
				id: "2"
			},
			{
				answerText: "Painat kaasua ja kiroat kissan alimpaan helv..",
				category: "Ennakoi elämäsi ehtoota",
				id: "3"
			},
		],
	},
	{
		// question #2
		questionText: "Mitä seuraavista vaihtoehdoista pelkäät eniten kuolemassa?",
		questionURL: "assets/comp-question-2.jpg",
		questionAlt: "Pelonsekainen hetki nurkassa",
		answerOptions: [
			{
				answerText: "Jälkeläiseni tappelevat perinnöstäni.",
				category: "Kun jotain sattuu",
				id: "1"
			},
			{
				answerText: "Elämättä jäänyttä elämääni.",
				category: "Unelmat näkyväksi",
				id: "2"
			},
			{
				answerText: "Miten läheiseni pärjäävät ilman minua.",
				category: "Ikääntymisturva",
				id: "3"
			},
		],
	},
	{
		// question #3
		questionText: "Mitä toivot jättäväsi jälkeesi?",
		questionURL: "assets/comp-question-3.jpg",
		questionAlt: "Naisoletettu katselee horisonttiin",
		answerOptions: [
			{
				answerText: "En mitään. Minulle riittää, että haihdun tuhkana tuuleen.",
				category: "Ikääntymisturva",
				id: "1"
			},
			{
				answerText: "Koottuja elämänviisauksiani.",
				category: "Ennakoi elämäsi ehtoota",
				id: "2"
			},
			{
				answerText: "Turvatun talouden läheisilleni.",
				category: "Kun jotain sattuu",
				id: "3"
			},
			{
				answerText: "Kultaisia muistoja.",
				category: "Unelmat näkyväksi",
				id: "4"
			},
		],
	},
	{
		// question #4
		questionText: "Valitse mieluisin lepopaikkasi.", // TODO kuvat vaihtoehtoina
		questionURL: "assets/comp-question-4.jpg",
		questionAlt: "Syksyinen metsä",
		answerOptions: [
			{
				answerText: "",
				answerImg: "assets/comp-answer-4-a.jpg",
				alt: "Krypta", // Krypta joka on jossain marilyn monroen vieressä - henkinen kuva.
				category: "Ikääntymisturva",
				id: "1"
			},
			{
				answerText: "",
				answerImg: "assets/comp-answer-4-d.jpg",
				alt: "Tykki jolla jäänteet ammutaan maailmaan", // Hunter S. Thompsonin innoittama tykki jolla jäänteet ammutaan maailmaan
				category: "Ennakoi elämäsi ehtoota",
				id: "2"
			},
			{
				answerText: "",
				answerImg: "assets/comp-answer-4-b.jpg",
				alt: "Pyramidi",
				category: "Unelmat näkyväksi",
				id: "3"
			},
			{
				answerText: "",
				answerImg: "assets/comp-answer-4-c.jpg",
				alt: "Suomalais-klassinen uurnalehto",
				category: "Ikääntymisturva",
				id: "4"
			},
		],
	},
	{
		// question #5
		questionText: "Millaisena näet unelmien vanhuutesi?",
		questionURL: "assets/comp-question-5.jpg",
		questionAlt: "Ikäihmiset elämöivät sohvalla peliohjaimet käsissänsä",
		answerOptions: [
			{
				answerText: "Asun idyllisessä torpassa elontieni loppuun saakka.",
				category: "Ikääntymisturva",
				id: "1"
			},
			{
				answerText:
					"Olen viherpeukalo ja aktiivinen osa omavaraista boheemia senioriyhteisöä.",
				category: "Ikääntymisturva",
				id: "2"
			},
			{
				answerText: "Asun ulkomailla ja aloitan aamuni raikkaalla mojitolla.",
				category: "Ikääntymisturva",
				id: "3"
			},
		],
	},
	{
		// question #6
		questionText: "Miten pidät mielenvireyttäsi yllä?",
		questionURL: "assets/comp-question-6.jpg",
		questionAlt: "Käsissä on kirja",
		answerOptions: [
			{
				answerText: "Elän vaarallisesti ja harrastan extreme-lajeja.",
				category: "Kun jotain sattuu",
				id: "1"
			},
			{
				answerText:
					"Täytän sudokuja, ristikoita ja kudon lapsenlapsilleni villasukkia.",
				category: "Kun jotain sattuu",
				id: "2"
			},
			{
				answerText:
					"Etsin ikuista nuoruuden lähdettä testaten uusimpia hyvinvointitrendejä.",
				category: "Ennakoi elämäsi ehtoota",
				id: "3"
			},
			{
				answerText:
					"Olen kirjaston kanta-asiakas ja seuraan useita eri tv-sarjoja.",
				category: "Unelmat näkyväksi",
				id: "4"
			},
		],
	},
	{
		// question #7
		questionText: "Valitse eniten sinua kuvaava valokuva", // kuvat TODO
		questionURL: "assets/comp-question-7.jpg",
		questionAlt: "Kuvaaja osoittamassa kameralla katsojaa kohti",
		answerOptions: [
			{
				answerText:
					"Olen tuuliviiri yllä talon katon, joka saa osakseen myös ajoittaista matalapainetta.",
				answerImg: "assets/comp-answer-7-a.jpg",
				alt: "Klassinen kukkotuuliviiri katolla.",
				category: "Kun jotain sattuu",
				id: "1"
			},
			{
				answerText:
					"Olen rauhan ja rakkauden tyyssija. En koskaan ajaudu ristiriitoihin.",
				answerImg: "assets/comp-answer-7-c.jpg",
				alt: "Lootusasento", // LOOTUSASENTO
				category: "Unelmat näkyväksi.",
				id: "2"
			},
			{
				answerText: "Teen päätöksiä ja pysyn niissä!",
				answerImg: "assets/comp-answer-7-b.jpg",
				alt: "Kivi",
				category: "Ikääntymisturva",
				id: "3"
			},
		],
	},
];

export default questions;

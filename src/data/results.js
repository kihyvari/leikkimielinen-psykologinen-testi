const results = [
	{
		resultCategory: "unelmatNakyviksi",
		resultTitle: "Unelmat näkyviksi",
		resultText:
			"Rakastat seikkailuja ja olet oman elämäsi Indiana Jones. Olet nähnyt Kiribatin aavikot ja Tetsuanin-lahden tyrskyisät kalliot. Mikään kristallikallo ei ole jäänyt sinulta kääntämättä. Vai onko sittenkään...? Tutkimaton tie ja seuraava mysteeri voi olla lähempänä kuin arvaatkaan - sielusi sopukoissa.",
		resultImg: "/assets/comp-result-1-unelmatNakyvaksi.jpg",
		resultAlt: "Ruoskamies, tottakai" /* TODO actual alts */,
		resultUrl: "https://www.pilviisi.fi/unelmat-nakyvaksi/",
		resultShareUrl: "https://leikkimielisen-testin-demo.web.app/lopputulos/unelmat-nakyviksi",
		resultPath: "/lopputulos/unelmat-nakyviksi",
		resultQuote: "Lopputulemani: Rakastat seikkailuja ja olet oman elämäsi Indiana Jones."
	},
	{
		resultCategory: "kunJotainSattuu",
		resultTitle: "Kun jotain sattuu",
		resultText:
			"Olet oman elämäsi supersankari! Aurinko laskee selkäsi taa. Itse kierrätysbambusta kutomasi laskuvarjon soljet vähän natisevat, mutta et ole huolissasi. Varavarjosi sentään on serkkusi Lefan kansalaisopistossa puukomposiitista rakentama super-ratkaisu. Ja vaikka se nyt ei toimisi ihan täydellisesti, läheisesti eivät joutuisi pulaan, koska sinulla on suunnitelma, joka toteutuu riippumatta siitä miten tässä käy. Se on ihan fifty-sixty. Kerran tässä vain eletään. Ja sydänkin kestää läpi ihmisen elämän. Uhkarohkeudestasi huolimatta kaikilla asioilla ei ole pakko pelata venäläistä rulettia vaan voit ennakoida ja laittaa oikeudelliset asiasi kuntoon.",
		resultImg: "/assets/comp-result-2-kunJotainSattuu.jpg",
		resultAlt: "Laskuvarjohyppy!" /* TODO actual alts */,
		resultUrl: "https://www.pilviisi.fi/kun-jotain-sattuu",
		resultShareUrl: "https://leikkimielisen-testin-demo.web.app/lopputulos/kun-jotain-sattuu",
		resultPath: "/lopputulos/kun-jotain-sattuu",
		resultQuote: "Lopputulemani: Olet oman elämäsi supersankari!",
	},
	{
		resultCategory: "ennakoiElamasiEhtoota",
		resultTitle: "Ennakoi elämäsi ehtoota",
		resultText:
			"Olet oman elämäsi mestarietsivä! Köysi kiristyy. Harmaat aivosolut raksuttavat, mutta kuka on Paroni Von Chlundenhauzenin myrkytyksen takana? Hovimestari? Ehkä? Entäpä  tuo epäilyttävän näköinen sisäkkö? Sinun on aika ennakoida muussakin kuin mysteerien ratkomisessa, rakas Watson.",
		resultImg: "/assets/comp-result-3-ennakoiElamasiEhtoota.jpg",
		resultAlt:
			"Poirot henkinen kuva. Monokkeli, kävelykeppi jne gubbe" /* TODO actual alts */,
		resultUrl: "https://www.pilviisi.fi/ennakoi-elamasi-ehtoota/",
		resultShareUrl: "https://leikkimielisen-testin-demo.web.app/lopputulos/ennakoi-elamasi-ehtoota",
		resultPath: "/lopputulos/ennakoi-elamasi-ehtoota",
		resultQuote: "Lopputulemani: Olet oman elämäsi mestarietsivä!",
	},
	{
		resultCategory: "ikaantymisturva",
		resultTitle: "Ikääntymisturva",
		resultText:
			"Olet oman onnesi Seppä ISOLLA ÄSSÄLLÄ. Tiedät mitä haluat ja toimit sen mukaan. Metallican paita päälle ja menoksi. Ai mihin? No Yano’s- baariin Tauskin keikalle tietysti, joka sijaitsee lähellä Fuengirolan-asuntoasi. Mikäs siellä on paistatellessa hyvässä säässä. Aiot kuulemma valmistaa kesällä Suomessa käydessäsi maailman suurimman puolukkapiirakan ja päästä Guinnesin ennätystenkirjaan. Kylmäpihlajan majakkahotelli on varattu syksyltä koko suvulle. Talvella sitten jotain vähän hillitympää. Alberton ja Miquelin alastonmaalaukset hiilitöinä. Voisit kuitenkin varautua pahan päivän varalle tekemällä tahtosi näkyväksi, jotta voit elää näköistäsi elämää silloinkin kun joku toinen pitää elämäsi ohjaksia käsissään.",
		resultImg: "/assets/comp-result-4-ikaantymisturva.jpg",
		resultAlt:
			"Metallica-paitainen mummo ja alastomia hahmoja ympärillä. Myös piirakka." /* TODO actual alts */,
		resultUrl: "https://www.pilviisi.fi/ikaantymisturva-ela-nakoistasi-elamaa/",
		resultShareUrl: "https://leikkimielisen-testin-demo.web.app/lopputulos/ikaantymisturva",
		resultPath: "/lopputulos/ikaantymisturva",
		resultQuote: "Lopputulemani: Olet oman onnesi Seppä ISOLLA ÄSSÄLLÄ.",
	},
];

export default results;

import React, { useState, useEffect } from "react";
import { useTransition, useSpring, animated, config } from "react-spring";
/* import { useMeasure } from "react-use"; */

const AnimatedImage = ({
	imageSrc,
	currentQuestion,
	endResult,
	showEndResult,
}) => {
	const imageHeights = [
		"130vw",
		"130vw",
		"60vw",
		"60vw",
		"60vw",
		"60vw",
		"130vw",
		"60vw",
	];

	const [index, setIndex] = useState(0);
	const [imageHeight, setImageHeight] = useState(imageHeights[index]);
	const [maxImageHeight, setMaxImageHeight] = useState("600px");

	const bgTransition = useSpring({
		height: imageHeight,
		maxHeight: maxImageHeight,
		config: { tension: 170, friction: 24 },
	});

	const imageTransition = useTransition(imageSrc, (item) => item, {
		from: { opacity: 0 },
		enter: { opacity: 1 },
		leave: { opacity: 0 },
		config: config.gentle,
	});

	// HACKY useEffects, because I did not bother doing things right
	useEffect(() => {
		const nextIndex = index + 1;
		if (index < 8) {
			setIndex(nextIndex);
		}
	}, [currentQuestion]); // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		if (index < 8) {
			setImageHeight(imageHeights[index]);
		}
	}, [index]); // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		if (showEndResult === true && endResult === "unelmatNakyviksi") {
			/* setImageHeight("130vw");
			setMaxImageHeight("600px"); */
			setIndex(1);
		} else if (showEndResult === true && endResult === "kunJotainSattuu") {
			/* setImageHeight("60vw");
			setMaxImageHeight("400px"); */
			setIndex(2);
		} else if (
			showEndResult === true &&
			endResult === "ennakoiElamasiEhtoota"
		) {
			/* setImageHeight("130vw");
			setMaxImageHeight("600px"); */
			setIndex(1);
		} else if (showEndResult === true && endResult === "ikaantymisturva") {
			/* setImageHeight("60vw");
			setMaxImageHeight("400px"); */
			setIndex(2);
		}
	}, [endResult, showEndResult]); // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		imageHeight === "130vw"
			? setMaxImageHeight("600px")
			: setMaxImageHeight("400px");
	}, [imageHeight]); // eslint-disable-line react-hooks/exhaustive-deps

	/* useEffect(() => {
		(currentQuestion !== 0 && currentQuestion !== 5)
			? setImageHeight(smallHeight)
			: setImageHeight(defaultHeight);
	}, [currentQuestion]); */

	// sets initial heigh & resizing event listener
	/* useEffect(() => {
		setImageHeight(height);
		window.addEventListener("resize", setImageHeight(height));
		return window.removeEventListener("resize", setImageHeight(height));
	}, [height]); */

	return (
		// <div className="animation-wrapper">
		<animated.div className="question-animation" style={bgTransition}>
			{imageTransition.map(({ item, props, key }) => (
				<animated.img style={props} key={key} src={item}></animated.img>
			))}
		</animated.div>
		// </div>
	);
};

export default AnimatedImage;

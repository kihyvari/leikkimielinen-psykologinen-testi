import { useEffect, useState } from "react";

const LoadingScreen = () => {
	const [load, setLoad] = useState(true);

	// fake authenticate for pre-loading reasons
	const authenticate = () => {
		return new Promise((resolve) => setTimeout(resolve, 2000)); // 2 seconds
	};

	// removes pre-loaded-DOM-loading-screen
	useEffect(() => {
		if (load) {
			authenticate().then(() => {
				const ele = document.getElementById("ipl-progress-indicator");
				if (ele) {
					// fade out
					ele.classList.add("available");
					setTimeout(() => {
						// remove from DOM
						ele.remove();
					}, 2000);
					setLoad(false);
				}
			});
		}
	}, [load]);
};

export default LoadingScreen;

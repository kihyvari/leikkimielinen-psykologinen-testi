import React, { Suspense, useEffect } from "react";

import CircularProgress from "@material-ui/core/CircularProgress";
import TestButton2 from "../buttons/Test-button-2";
import StartButton from "../buttons/StartButton";
import { useHistory } from "react-router-dom";

const AnimatedButtonImage = React.lazy(() =>
	import("../AnimatedButtonImage.js")
); // TODO animation for image answer buttons here */

const AnswerSection = ({
	currentQuestion,
	questions,
	score,
	setScore,
	setCurrentQuestion,
	setShowEndResult,
	showEndResult,
	endResultPath,
	showIntro,
	handleStart,
	handleEnnakoi,
	handleIkaantymis,
	handleSattuu,
	handleUnelmat,
}) => {
	let newScore = 0;
	let history = useHistory();
	const buttonImageList = [
		"assets/comp-answer-4-a.jpg",
		"assets/comp-answer-4-b.jpg",
		"assets/comp-answer-4-c.jpg",
		"assets/comp-answer-4-d.jpg",
		"assets/comp-answer-7-a.jpg",
		"assets/comp-answer-7-b.jpg",
		"assets/comp-answer-7-c.jpg",
	];

	// preloading images
	useEffect(() => {
		buttonImageList.forEach((bImage) => {
			new Image().src = bImage;
		});
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	const handleAnswerClick = (category) => {
		if (category === "Unelmat näkyväksi") {
			newScore = score.unelmatNakyviksi + 1;
			setScore((prev) => ({
				...prev,
				unelmatNakyviksi: newScore,
			}));
			console.log("Unelmat!"); // TODO clean up
		} else if (category === "Kun jotain sattuu") {
			let newScore = score.kunJotainSattuu + 1;
			setScore((prev) => ({
				...prev,
				kunJotainSattuu: newScore,
			}));
			console.log("Sattuu!");
		} else if (category === "Ennakoi elämäsi ehtoota") {
			let newScore = score.ennakoiElamasiEhtoota + 1;
			setScore((prev) => ({
				...prev,
				ennakoiElamasiEhtoota: newScore,
			}));
			console.log("Ennakoi!");
		} else {
			let newScore = score.ikaantymisturva + 1;
			setScore((prev) => ({
				...prev,
				ikaantymisturva: newScore,
			}));
			console.log("Ikäänny!");
		}

		const nextQuestion = currentQuestion + 1;
		if (nextQuestion < questions.length) {
			setCurrentQuestion(nextQuestion);
		} else {
			setShowEndResult(true);
			history.push(endResultPath);
		}
	};
	// in case of intro
	if (showIntro) {
		return (
			<div className="description">
				<StartButton handleStart={handleStart} />
				<h3>Lopputulemia testitarkoitusta varten</h3>
				<div className="button-wrapper">
					<TestButton2 onClick={handleUnelmat}>Unelmat Näkyviksi</TestButton2>
					<TestButton2 onClick={handleSattuu}>Kun Jotain Sattuu</TestButton2>
					<TestButton2 onClick={handleEnnakoi}>
						Ennakoi Elämäsi Ehtoota
					</TestButton2>
					<TestButton2 onClick={handleIkaantymis}>Ikääntymisturva</TestButton2>
				</div>
			</div>
		);
		// in case it's time to show end results
		/* } else if (showEndResult) {
		
		return (
			<Redirect to="/lopputulos" />
		) */
	} else if (currentQuestion === 3 || currentQuestion === 6) {
		// checks if question has image type answers
		return (
			<div className="img-answer-section">
				<Suspense fallback={<CircularProgress className="loading-circle" />}>
					{questions[currentQuestion].answerOptions.map((answerOption) => (
						<AnimatedButtonImage
							className="answer-img-button"
							key={answerOption.id}
							onClick={() => {
								handleAnswerClick(answerOption.category);
							}}
							imageSrc={answerOption.answerImg}
							alt={answerOption.alt}
						/>
					))}
				</Suspense>
			</div>
		);
	} else {
		return (
			<div className="answer-section">
				{questions[currentQuestion].answerOptions.map((answerOption) => (
					<button
						className="start-and-reset-button"
						key={answerOption.answerText}
						onClick={() => {
							handleAnswerClick(answerOption.category);
						}}
					>
						{answerOption.answerText}
					</button>
				))}
			</div>
		);
	}
};

export default AnswerSection;

import React from "react";
import CallToActionButton from "../../buttons/CallToActionButton";
import results from "../../data/results";
import ResetButton from "../../buttons/ResetButton";
import AnimatedImage from "../../AnimatedImage";
import {
	FacebookShareButton,
	FacebookIcon,
	WhatsappShareButton,
	WhatsappIcon,
} from "react-share";

const EnnakoiElamasiEhtoota = ({endResult, endResultTitle, endResultIndex, endResultUrl, showEndResult, handleReset}) => {
    /* const shareUrl = "https://leikkimielisen-testin-demo.web.app/"; */

	const goToLink = () => {
		/* const buttonURL = results[endResultIndex].resultURL */
		window.open(results[2].resultUrl); // TODO conditional/dynamical url
	};

	return (
		<div className="result-wrapper">
			<h3>
				Lopputulemasi: <br />
				<span className="result-title">
					{results[2].resultTitle}
				</span>
			</h3>
			<AnimatedImage
				className="result-img"
				imageSrc={results[2].resultImg}
				alt={results[2].resultAlt}
				endResult={endResult}
				showEndResult={showEndResult}
			/>
			<p>
				{results[2].resultText} <br />
			</p>
			<div className="button-wrapper">
				<CallToActionButton onClick={goToLink}>
					Voisit hyötyä palvelustamme {results[2].resultTitle}
				</CallToActionButton>
				<ResetButton handleReset={handleReset} />
			</div>
			<div className="some-container">
				<FacebookShareButton
					url={results[2].resultShareUrl}
					quote={results[2].resultQuote}
					hashtag="#pilviisi"
					className="share-button"
				>
					<FacebookIcon size={64} round />
				</FacebookShareButton>
				<WhatsappShareButton
					url={results[2].resultShareUrl}
					title={results[2].resultQuote}
					className="share-button"
				>
					<WhatsappIcon size={64} round />
				</WhatsappShareButton>
			</div>
		</div>
	);
};

export default EnnakoiElamasiEhtoota;
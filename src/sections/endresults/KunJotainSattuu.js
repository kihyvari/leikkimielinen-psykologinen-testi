import React from "react";
import CallToActionButton from "../../buttons/CallToActionButton";
import results from "../../data/results";
import ResetButton from "../../buttons/ResetButton";
import AnimatedImage from "../../AnimatedImage";
import {
	FacebookShareButton,
	FacebookIcon,
	WhatsappShareButton,
	WhatsappIcon,
} from "react-share";

const KunJotainSattuu = ({
	endResult,
	endResultUrl,
	showEndResult,
	handleReset,
}) => {
	/* const shareUrl = "https://leikkimielisen-testin-demo.web.app/lopputulos/kun-jotain-sattuu"; */

	const goToLink = () => {
		/* const buttonURL = results[endResultIndex].resultURL */
		window.open(results[1].resultUrl); // TODO conditional/dynamical url
	};

	return (
		<div className="result-wrapper">
			<h3>
				Lopputulemasi: <br />
				<span className="result-title">{results[1].resultTitle}</span>
			</h3>
			<AnimatedImage
				className="result-img"
				imageSrc={results[1].resultImg}
				alt={results[1].resultAlt}
				endResult="kunJotainSattuu"
				showEndResult={true}
			/>
			<p>
				{results[1].resultText} <br />
			</p>
			<div className="button-wrapper">
				<CallToActionButton onClick={goToLink}>
					Voisit hyötyä palvelustamme {results[1].resultTitle}
				</CallToActionButton>
				<ResetButton handleReset={handleReset} />
			</div>
			<div className="some-container">
				<FacebookShareButton
					url={results[1].resultShareUrl}
					quote={results[1].resultQuote}
					hashtag="#pilviisi"
					className="share-button"
				>
					<FacebookIcon size={64} round />
				</FacebookShareButton>
				<WhatsappShareButton
					url={results[1].resultShareUrl}
					title={results[1].resultQuote}
					className="share-button"
				>
					<WhatsappIcon size={64} round />
				</WhatsappShareButton>
			</div>
		</div>
	);
};

export default KunJotainSattuu;

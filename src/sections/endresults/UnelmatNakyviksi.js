import React from "react";
import CallToActionButton from "../../buttons/CallToActionButton";
import results from "../../data/results";
import ResetButton from "../../buttons/ResetButton";
import AnimatedImage from "../../AnimatedImage";
import {
	FacebookShareButton,
	FacebookIcon,
	WhatsappShareButton,
	WhatsappIcon,
} from "react-share";

const UnelmatNakyviksi = ({endResult, endResultTitle, endResultIndex, endResultUrl, showEndResult, handleReset, /* setEndResultPath */}) => {
	/* const shareUrl = "https://leikkimielisen-testin-demo.web.app/lopputulos/unelmat-nakyviksi"; */

	const goToLink = () => {
		/* const buttonURL = results[endResultIndex].resultURL */
		window.open(results[0].resultUrl); // TODO conditional/dynamical url
	};

	return (
		<div className="result-wrapper">
			<h3>
				Lopputulemasi: <br />
				<span className="result-title">
				{results[0].resultTitle}
				</span>
			</h3>
			<AnimatedImage
				className="result-img"
				imageSrc={results[0].resultImg}
				alt={results[0].resultAlt}
				endResult="unelmatNakyviksi"
				showEndResult={showEndResult}
			/>
			<p>
			{results[0].resultText} <br />
			</p>
			<div className="button-wrapper">
				<CallToActionButton onClick={goToLink}>
					Voisit hyötyä palvelustamme {results[0].resultTitle}
				</CallToActionButton>
				<ResetButton handleReset={handleReset} />
			</div>
			<div className="some-container">
				<FacebookShareButton
					url={results[0].resultShareUrl}
					quote={results[0].resultQuote}
					hashtag="#pilviisi"
					className="share-button"
				>
					<FacebookIcon size={64} round />
				</FacebookShareButton>
				<WhatsappShareButton
					url={results[0].resultShareUrl}
					title={results[0].resultQuote}
					className="share-button"
				>
					<WhatsappIcon size={64} round />
				</WhatsappShareButton>
			</div>
		</div>
	);
};

export default UnelmatNakyviksi;
import React/* , { useState, useEffect } */ from "react";
import CallToActionButton from "../../buttons/CallToActionButton";
import results from "../../data/results";
import ResetButton from "../../buttons/ResetButton";
import AnimatedImage from "../../AnimatedImage";
import {
	/* EmailShareButton,
	EmailIcon, */
	FacebookShareButton,
	FacebookIcon,
	WhatsappShareButton,
	WhatsappIcon,
} from "react-share";

const Ikaantymisturva = ({endResult, endResultTitle, endResultIndex, endResultUrl, showEndResult, handleReset}) => {
	/* const shareUrl = "https://leikkimielisen-testin-demo.web.app/"; */

	const goToLink = () => {
		/* const buttonURL = results[endResultIndex].resultURL */
		window.open(results[3].resultUrl); // TODO conditional/dynamical url
	};

	return (
		<div className="result-wrapper">
			<h3>
				Lopputulemasi: <br />
				<span className="result-title">
					{results[3].resultTitle}
				</span>
			</h3>
			<AnimatedImage
				className="result-img"
				imageSrc={results[3].resultImg}
				alt={results[3].resultAlt}
				endResult="ikaantymisturva"
				showEndResult={true}
			/>
			<p>
				{results[3].resultText} <br />
			</p>
			<div className="button-wrapper">
				<CallToActionButton onClick={goToLink}>
					Voisit hyötyä palvelustamme {results[3].resultTitle}
				</CallToActionButton>
				<ResetButton handleReset={handleReset} />
			</div>
			<div className="some-container">
				<FacebookShareButton
					url={results[3].resultShareUrl}
					quote={results[3].resultQuote}
					hashtag="#pilviisi"
					className="share-button"
				>
					<FacebookIcon size={64} round />
				</FacebookShareButton>
				<WhatsappShareButton
					url={results[3].resultShareUrl}
					title={results[3].resultQuote}
					className="share-button"
				>
					<WhatsappIcon size={64} round />
				</WhatsappShareButton>
			</div>
		</div>
	);
};

export default Ikaantymisturva;
import React, { Suspense, useEffect } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
/* import TestButton2 from "../buttons/Test-button-2";
import StartButton from "../buttons/StartButton"; */

const AnimatedImage = React.lazy(() => import("../AnimatedImage.js"));

const QuestionSection = ({
	currentQuestion,
	questions,
	showIntro,
	endResult,
	/* handleStart,
	handleEnnakoi,
	handleIkaantymis,
	handleSattuu,
	handleUnelmat, */
}) => {
	
	
	const imageList = questions.map(imageSrc => imageSrc.questionURL)
	// preloading images
	useEffect(() => {
		imageList.forEach((image) => {
			new Image().src = image;
		});
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<span>
			{showIntro ? (
				<div className="description">
					<h1>KUKA ON PUIKOISSA? </h1>
					<h2>HUPAINEN PERSOONALLISUUSTESTI</h2>
					{/* <Suspense fallback={<CircularProgress />}> */}
					{/* {transitions.map(
						({ item, key, props }) =>
							item && <AnimatedSuspenseImg key={key} style={props} />
					)} */}
					{/* </Suspense> */}
					{/* 					<CircularProgress  className="loading-circle" /> */}
					<p>
						Mikä on elämäsi tarkoitus, siinäpä vasta pulma. Oletko enemmän oman
						elämäsi kuski vai kyytiläinen? Ota siitä selvää.
					</p>
					<Suspense fallback={<CircularProgress className="loading-circle" />}>
						<AnimatedImage
							imageSrc="assets/comp-intro.jpg"
							className="question-img"
							endResult={endResult}
						/>
					</Suspense>
				</div>
			) : (
				<div className="question-section">
					<div className="question-count">
						<span>Kysymys {currentQuestion + 1}</span>/{questions.length}
					</div>
					<div className="question-text">
						{questions[currentQuestion].questionText}
					</div>
					<Suspense fallback={<CircularProgress className="loading-circle" />}>
						<AnimatedImage
							className="question-img"
							imageSrc={questions[currentQuestion].questionURL}
							alt={questions[currentQuestion].questionAlt}
							currentQuestion={currentQuestion}
						/>
					</Suspense>
				</div>
			)}
		</span>
	);
};

export default QuestionSection;

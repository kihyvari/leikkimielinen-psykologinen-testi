import React /* { useState, useEffect } */ from "react";
import { /* Switch, */ Route, /* Redirect */ } from "react-router-dom";
/* import CallToActionButton from "../buttons/CallToActionButton"; */
/* import results from "../data/results"; */
/* import ResetButton from "../buttons/ResetButton"; */
/* import AnimatedImage from "../AnimatedImage"; */
/* import {
	EmailShareButton,
	EmailIcon,
	FacebookShareButton,
	FacebookIcon,
	FacebookMessengerShareButton,
	FacebookMessengerIcon,
	WhatsappShareButton,
	WhatsappIcon,
	TwitterShareButton,
	TwitterIcon,
} from "react-share"; */
import EnnakoiElamasiEhtoota from "./endresults/EnnakoiElamasiEhtoota";
import Ikaantymisturva from "./endresults/Ikaantymisturva";
import KunJotainSattuu from "./endresults/KunJotainSattuu";
import UnelmatNakyviksi from "./endresults/UnelmatNakyviksi";

const EndResultComponent = ({ endResult, handleReset, endResultUrl, endResultIndex, endResultTitle, showEndResult }) => {
	/* const [endResultUrl, setEndResultUrl] = useState("");
	const [endResultTitle, setEndResultTitle] = useState("");
	const [endResultPath, setEndResultPath] = useState("")
	
	const endResultIndex = results.findIndex(
		(result) => result.resultCategory === endResult
	);
	

	useEffect(() => {
		setEndResultUrl(results[endResultIndex].resultURL);
		setEndResultTitle(results[endResultIndex].resultTitle);
		setEndResultPath(results[endResultIndex].resultPath);
		console.log("endResult.js findEndResult: " + endResultIndex);
		console.log("endResult.js endResult: " + endResult);
	}, [showEndResult]); // might be obsolete TODO */

	return (
		<>
		{/* <Switch> */}

			<Route path="/lopputulos/ennakoi-elamasi-ehtoota">
				<EnnakoiElamasiEhtoota
					endResult={endResult}
					endResultUrl={endResultUrl}
					endResultTitle={endResultTitle}
					endResultIndex={endResultIndex}
					handleReset={handleReset}
					showEndResult={showEndResult}
				/>
			</Route>
			<Route path="/lopputulos/ikaantymisturva">
				<Ikaantymisturva
					endResult={endResult}
					endResultUrl={endResultUrl}
					endResultTitle={endResultTitle}
					endResultIndex={endResultIndex}
					handleReset={handleReset}
					showEndResult={showEndResult}
				/>
			</Route>
			<Route path="/lopputulos/kun-jotain-sattuu">
				<KunJotainSattuu
					endResult={endResult}
					endResultUrl={endResultUrl}
					endResultTitle={endResultTitle}
					endResultIndex={endResultIndex}
					handleReset={handleReset}
					showEndResult={showEndResult}
				/>
			</Route>
			<Route path="/lopputulos/unelmat-nakyviksi">
				<UnelmatNakyviksi
					endResult={endResult}
					endResultUrl={endResultUrl}
					endResultTitle={endResultTitle}
					endResultIndex={endResultIndex}
					handleReset={handleReset}
					showEndResult={showEndResult}
				/>
			</Route>
			{/* </Switch> */}
		</>
	);
};

export default EndResultComponent;

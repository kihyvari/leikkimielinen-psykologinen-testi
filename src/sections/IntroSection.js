import React, { Suspense, /* useState, useEffect */ } from "react";
/* import { Skeleton } from "@material-ui/lab"; */
/* import TestButton1 from "./Test-button-1"; */
import CircularProgress from "@material-ui/core/CircularProgress";
/* import { useSpring, useTransition, animated, config } from "react-spring"; */
/* import { Transition } from "react-transition-group"; */
import TestButton2 from "../buttons/Test-button-2";
import StartButton from "../buttons/StartButton";
/* import styled from "styled-components"; */
/* import AnimatedImage from "../AnimatedImage"; */

const AnimatedImage = React.lazy(() => import("../AnimatedImage.js"));

/* const AnimatedImage = ({ imageSrc }) => {
		const imageTransition = useTransition(imageSrc, (item) => item, {
			from: { opacity: 0 },
			enter: { opacity: 1 },
			leave: { opacity: 0 },
			config: config.molasses,
		});

		return (
			
			<div className="bg">
			  {imageTransition.map(({ item, props, key }) => (
				<animated.img style={props} key={key} src={item}></animated.img>
			  ))}
			</div>
		  );
		}; */

const IntroSection = ({
	handleStart,
	handleEnnakoi,
	handleIkaantymis,
	handleSattuu,
	handleUnelmat,
}) => {

	return (
		<div className="description">
			<h1>KUKA ON PUIKOISSA? </h1>
			<h2>HUPAINEN PERSOONALLISUUSTESTI</h2>
			<p>
				Mikä on elämäsi tarkoitus, siinäpä vasta pulma. Oletko enemmän oman
				elämäsi kuski vai kyytiläinen? Ota siitä selvää.
			</p>
			<Suspense fallback={<CircularProgress />}>
				<AnimatedImage
					imageSrc="assets/comp-intro.jpg"
					className="question-img"
				/>
			</Suspense>
			<StartButton handleStart={handleStart} />
			<h3>Lopputulemia testitarkoitusta varten</h3>
			<div className="button-wrapper">
				{/* <button onClick={() => set(index === 0 ? 1 : 0)}>change</button> */}
				<TestButton2 onClick={handleUnelmat}>Unelmat Näkyviksi</TestButton2>
				<TestButton2 onClick={handleSattuu}>Kun Jotain Sattuu</TestButton2>
				<TestButton2 onClick={handleEnnakoi}>
					Ennakoi Elämäsi Ehtoota
				</TestButton2>
				<TestButton2 onClick={handleIkaantymis}>Ikääntymisturva</TestButton2>
			</div>
		</div>
	);
};

export default IntroSection;

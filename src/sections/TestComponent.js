import React from "react";
/* import { Skeleton } from "@material-ui/lab"; */
import questions from "../data/questions";
/* import IntroSection from "./IntroSection"; */
import AnswerSection from "./AnswerSection";
import QuestionSection from "./QuestionSection";
import {
	useHistory,
	/* BrowserRouter, */
} from "react-router-dom";

/* const IntroSection = React.lazy(() =>
	import("./IntroSection")
); */

const TestComponent = ({
	showIntro,
	setShowIntro,
	currentQuestion,
	score,
	setScore,
	setCurrentQuestion,
	setShowEndResult,
	setEndResult,
	endResult,
	endResultPath,
	showEndResult,
	handleStart,
}) => {

	// react-router useHistory for history reasons
	let history = useHistory();

	// Testing purpose TODO remove
	const handleUnelmat = () => {
		setScore({
			unelmatNakyviksi: 1,
			kunJotainSattuu: 0,
			ennakoiElamasiEhtoota: 0,
			ikaantymisturva: 0,
		});
		setCurrentQuestion(6);
		setEndResult("unelmatNakyviksi")
		setShowEndResult(true);
		setShowIntro(false);
		history.push("/lopputulos/unelmat-nakyviksi")
		console.log(score);
		console.log(endResult);
	};

	// Testing purpose TODO remove
	const handleSattuu = () => {
		setScore({
			unelmatNakyviksi: 0,
			kunJotainSattuu: 1,
			ennakoiElamasiEhtoota: 0,
			ikaantymisturva: 0,
		});
		setCurrentQuestion(6);
		setEndResult("kunJotainSattuu")
		setShowEndResult(true);
		setShowIntro(false);
		history.push("/lopputulos/kun-jotain-sattuu")
		console.log(score);
		console.log(endResult);
	};

	// Testing purpose TODO remove
	const handleEnnakoi = () => {
		setScore({
			unelmatNakyviksi: 0,
			kunJotainSattuu: 0,
			ennakoiElamasiEhtoota: 1,
			ikaantymisturva: 0,
		});
		setCurrentQuestion(6);
		setEndResult("ennakoiElamasiEhtoota")
		setShowEndResult(true);
		setShowIntro(false);
		history.push("/lopputulos/ennakoi-elamasi-ehtoota")
		console.log(score);
		console.log(endResult);
	};

	// Testing purpose TODO remove
	const handleIkaantymis = () => {
		setScore({
			unelmatNakyviksi: 0,
			kunJotainSattuu: 0,
			ennakoiElamasiEhtoota: 0,
			ikaantymisturva: 1,
		});
		setCurrentQuestion(6);
		setEndResult("ikaantymisturva")
		setShowEndResult(true);
		setShowIntro(false);
		history.push("/lopputulos/ikaantymisturva")
		console.log(score);
		console.log(endResult);
	};

	return (
			<>
				<QuestionSection
					showIntro={showIntro}
					setShowIntro={setShowIntro}
					currentQuestion={currentQuestion}
					questions={questions}
					endResult={endResult}
				/>
				<AnswerSection
					showIntro={showIntro}
					setShowIntro={setShowIntro}
					currentQuestion={currentQuestion}
					questions={questions}
					score={score}
					setScore={setScore}
					setCurrentQuestion={setCurrentQuestion}
					setShowEndResult={setShowEndResult}
					showEndResult={showEndResult}
					endResult={endResult}
					endResultPath={endResultPath}
					handleStart={handleStart}
					handleEnnakoi={handleEnnakoi}
					handleIkaantymis={handleIkaantymis}
					handleSattuu={handleSattuu}
					handleUnelmat={handleUnelmat}
				/>
			</>
	);
};

export default TestComponent;

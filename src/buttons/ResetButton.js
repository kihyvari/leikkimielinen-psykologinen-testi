import React from "react";


const ResetButton = ({
  handleReset
}) => {
  
  return (
    <button className="start-and-reset-button" onClick={handleReset}>
      Aloita Alusta!
    </button>
  );
};

export default ResetButton;

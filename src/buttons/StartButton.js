import React from "react";


const StartButton = ({
  handleStart
}) => {
  
  return (
    <button className="start-and-reset-button" onClick={handleStart}>
      Aloita Testi!
    </button>
  );
};

export default StartButton;
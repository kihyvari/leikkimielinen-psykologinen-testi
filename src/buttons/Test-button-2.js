import styled from 'styled-components';
// TODO remove styled components and merge this into .css
const SubmitButton = styled.button`
  padding-top: 16px;
  padding-right: 30px;
  padding-bottom: 16px;
  padding-left: 30px;
  font-size: 16px;
  font-weight: 700;
  border-radius: 15px;
  border: 0;
  background-color: #2d69ff;
  transition: background-color 0.15s, border-color 0.15s, color 0.15s;
  color: #fff;
  cursor: ${(props) => (props.disabled ? 'not-allowed' : 'pointer')};
  opacity: ${(props) => (props.disabled ? 0.6 : 1)};
  margin: 5px;
  color: ${(props) => (props.theme.$type === 'dark' ? '#5f95ff' : '#fff')};
  user-select: none;
  width: inherit;
  flex: 0 1 auto;

  &:hover {
    background-color: #e0c588;
    border-color: transparent;
    color: #333;
  }

  &:active {
    box-shadow: inset 0 1px 8px 2px rgba(0, 16, 32, 0.2);
    border-color: rgba(0, 16, 32, 0.2);
  }
`;

export default SubmitButton;
